#!/usr/bin/env bash

set -e
set -x

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src
etc_dir=etc

mkdir -p ${dist_dir}

rsync -aH ${etc_dir}/index.html ${dist_dir}
rsync -aH ${src_dir}/* ${dist_dir}

# RAAus Operations Manual 7.1

raaus_ops_manual71=5-om-71-august-2016-single-pages
raaus_ops_manual71_pdf=${raaus_ops_manual71}.pdf
raaus_ops_manual71_dist_dir=${dist_dir}/${raaus_ops_manual71}

if [ -f ${src_dir}/${raaus_ops_manual71_pdf} ]; then
  mkdir -p ${raaus_ops_manual71_dist_dir}

  pdftk ${src_dir}/${raaus_ops_manual71_pdf} cat 5-11 output ${raaus_ops_manual71_dist_dir}/table-of-contents.pdf
  pdftk ${src_dir}/${raaus_ops_manual71_pdf} cat 34-39 output ${raaus_ops_manual71_dist_dir}/2.01-pilot-certificate-group-a-and-b.pdf
  pdftk ${src_dir}/${raaus_ops_manual71_pdf} cat 47-49 output ${raaus_ops_manual71_dist_dir}/2.04-pilot-certificates-aeroplane-groups.pdf
  pdftk ${src_dir}/${raaus_ops_manual71_pdf} cat 50-51 output ${raaus_ops_manual71_dist_dir}/2.06-student-or-converting-pilot-certificate.pdf
  pdftk ${src_dir}/${raaus_ops_manual71_pdf} cat 52-61 output ${raaus_ops_manual71_dist_dir}/2.07-pilot-certificate-group-a-and-b.pdf
fi

# RAAus Operations Manual 7.1.1 (31 March 2021)

raaus_ops_manual711=raaus-operations-manual-issue-711
raaus_ops_manual711_pdf=${raaus_ops_manual711}.pdf
raaus_ops_manual711_dist_dir=${dist_dir}/${raaus_ops_manual711}

if [ -f ${src_dir}/${raaus_ops_manual711_pdf} ]; then
  mkdir -p ${raaus_ops_manual711_dist_dir}

  pdftk ${src_dir}/${raaus_ops_manual711_pdf} cat 4-5 output ${raaus_ops_manual711_dist_dir}/table-of-contents.pdf
  pdftk ${src_dir}/${raaus_ops_manual711_pdf} cat 23-27 output ${raaus_ops_manual711_dist_dir}/2.01-pilot-certificate-group-a-and-b.pdf
  pdftk ${src_dir}/${raaus_ops_manual711_pdf} cat 33 output ${raaus_ops_manual711_dist_dir}/2.04-pilot-certificates-aeroplane-groups.pdf
  pdftk ${src_dir}/${raaus_ops_manual711_pdf} cat 34 output ${raaus_ops_manual711_dist_dir}/2.04-pilot-qualifications.pdf
  pdftk ${src_dir}/${raaus_ops_manual711_pdf} cat 36-37 output ${raaus_ops_manual711_dist_dir}/2.06-student-or-converting-pilot-certificate.pdf
  pdftk ${src_dir}/${raaus_ops_manual711_pdf} cat 38-45 output ${raaus_ops_manual711_dist_dir}/2.07-pilot-certificate-group-a-and-b.pdf
fi

# RAAus Syllabus of Flight Training 7

raaus_syllabus_flight_training7=1-syllabus-of-flight-training-issue-7-v2-single-pages-1
raaus_syllabus_flight_training7_pdf=${raaus_syllabus_flight_training7}.pdf
raaus_syllabus_flight_training7_dist_dir=${dist_dir}/${raaus_syllabus_flight_training7}

if [ -f ${src_dir}/${raaus_syllabus_flight_training7_pdf} ]; then
  mkdir -p ${raaus_syllabus_flight_training7_dist_dir}

  pdftk ${src_dir}/${raaus_syllabus_flight_training7_pdf} cat 6-19 output ${raaus_syllabus_flight_training7_dist_dir}/table-of-contents.pdf
  pdftk ${src_dir}/${raaus_syllabus_flight_training7_pdf} cat 21-29 output ${raaus_syllabus_flight_training7_dist_dir}/1.01-group-a-3-axis-syllabus.pdf
  pdftk ${src_dir}/${raaus_syllabus_flight_training7_pdf} cat 54 output ${raaus_syllabus_flight_training7_dist_dir}/1.04-passenger-carrying-endorsement-syllabus.pdf
  pdftk ${src_dir}/${raaus_syllabus_flight_training7_pdf} cat 55 output ${raaus_syllabus_flight_training7_dist_dir}/1.05-cross-country-endorsement-syllabus.pdf
  pdftk ${src_dir}/${raaus_syllabus_flight_training7_pdf} cat 56-58 output ${raaus_syllabus_flight_training7_dist_dir}/1.06-formation-endorsement-syllabus.pdf
  pdftk ${src_dir}/${raaus_syllabus_flight_training7_pdf} cat 67-70 output ${raaus_syllabus_flight_training7_dist_dir}/1.08-low-level-endorsement-syllabus.pdf
  pdftk ${src_dir}/${raaus_syllabus_flight_training7_pdf} cat 101-117 output ${raaus_syllabus_flight_training7_dist_dir}/2.01-basic-aeronautical-knowledge-syllabus.pdf
  pdftk ${src_dir}/${raaus_syllabus_flight_training7_pdf} cat 118-120 output ${raaus_syllabus_flight_training7_dist_dir}/2.02-air-legislation-syllabus.pdf
  pdftk ${src_dir}/${raaus_syllabus_flight_training7_pdf} cat 121-127 output ${raaus_syllabus_flight_training7_dist_dir}/2.03-navigation-and-meteorology-syllabus.pdf
  pdftk ${src_dir}/${raaus_syllabus_flight_training7_pdf} cat 128-129 output ${raaus_syllabus_flight_training7_dist_dir}/2.04-radio-operator-syllabus.pdf
  pdftk ${src_dir}/${raaus_syllabus_flight_training7_pdf} cat 130-132 output ${raaus_syllabus_flight_training7_dist_dir}/2.05-human-factors-syllabus.pdf
fi

# RAAus Technical Manual 4

raaus_technical_manual4=2-raaus-technical-manual-issue-4-single-pages
raaus_technical_manual4_pdf=${raaus_technical_manual4}.pdf
raaus_technical_manual4_dist_dir=${dist_dir}/${raaus_technical_manual4}

if [ -f ${src_dir}/${raaus_technical_manual4_pdf} ]; then
  mkdir -p ${raaus_technical_manual4_dist_dir}

  pdftk ${src_dir}/${raaus_technical_manual4_pdf} cat 9-11 output ${raaus_technical_manual4_dist_dir}/table-of-contents.pdf
  pdftk ${src_dir}/${raaus_technical_manual4_pdf} cat 13-16 output ${raaus_technical_manual4_dist_dir}/abbreviations-and-definitions.pdf
  pdftk ${src_dir}/${raaus_technical_manual4_pdf} cat 43-51 output ${raaus_technical_manual4_dist_dir}/5.1-aircraft-registration.pdf
  pdftk ${src_dir}/${raaus_technical_manual4_pdf} cat 103-107 output ${raaus_technical_manual4_dist_dir}/12.1-daily-and-pre-flight-inspections.pdf
fi

# RAAus Technical Manual 4.1 (31 March 2021)

raaus_technical_manual41=raaus-technical-manual-issue-41
raaus_technical_manual41_pdf=${raaus_technical_manual41}.pdf
raaus_technical_manual41_dist_dir=${dist_dir}/${raaus_technical_manual41}

if [ -f ${src_dir}/${raaus_technical_manual41_pdf} ]; then
  mkdir -p ${raaus_technical_manual41_dist_dir}

  pdftk ${src_dir}/${raaus_technical_manual41_pdf} cat 7-8 output ${raaus_technical_manual41_dist_dir}/table-of-contents.pdf
  pdftk ${src_dir}/${raaus_technical_manual41_pdf} cat 9-11 output ${raaus_technical_manual41_dist_dir}/abbreviations-and-definitions.pdf
  pdftk ${src_dir}/${raaus_technical_manual41_pdf} cat 37-38 output ${raaus_technical_manual41_dist_dir}/5.1-aircraft-registration.pdf
  pdftk ${src_dir}/${raaus_technical_manual41_pdf} cat 78-82 output ${raaus_technical_manual41_dist_dir}/12.1-daily-and-pre-flight-inspections.pdf
fi

####

pdf_files=$(cd ${dist_dir} && find . -type f -name '*.pdf')

IFS=$'\n'
for pdf_file in $pdf_files
do
  basename=$(basename -- "${pdf_file}")
  dirname=$(dirname -- "${pdf_file}")
  filename="${basename%.*}"

  page1_pdf_dir=${dist_dir}/${dirname}
  page1_pdf="${page1_pdf_dir}/${filename}_page1.pdf"

  mkdir -p "${page1_pdf_dir}"

  gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile=${page1_pdf} ${dist_dir}/${pdf_file}

  page1_pdf_png="${page1_pdf}.png"

  convert ${page1_pdf} ${page1_pdf_png}

  for size in 600 450 350 250 150 100 65 45
  do
    page1_pdf_png_size="${page1_pdf}-${size}.png"
    convert ${page1_pdf_png} -resize ${size}x${size} ${page1_pdf_png_size}
  done
done
IFS="$OIFS"